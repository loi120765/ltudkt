from Document import Document


class ManageDocument():
  def __init__(self) -> None:
    super().__init__()
  
  def addDocument(doc: Document) -> None:
    pass

  def findDocument() -> list:
    pass

  def showDocuments() -> list:
    pass

  def sortDocumentByQty(isAsc: bool) -> list:
    pass

  def showDocumentsByReleaseMonth(month: int) -> list:
    pass

  def getDailyRevenuef(doc: Document) -> float:
    pass

  def sortDocumentByDailyRevenuef(isAsc: bool) -> list:
    pass

  def sortDocumentByDocumentName(isAsc: bool) -> list:
    pass

  def getTopDocuments() -> list:
    pass

  def editDocument(doc: Document) -> Document:
    pass

  def deleteDocument(doc: Document) -> None:
    pass