class Document():
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan) -> None:
    super().__init__()
    self.maTaiLieu = maTaiLieu
    self.nhaXuatBan = nhaXuatBan
    self.soLuongTonKho = soLuongTonKho
    self.soLuongBan = soLuongBan
    self.giaBan = giaBan

class Sach(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, tacGia, soTrang) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.tacGia = tacGia
    self.soTrang = soTrang

class TapChi(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, soPhatHanh, thangPhatHanh) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.soPhatHanh = soPhatHanh
    self.thangPhatHanh = thangPhatHanh

class Bao(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, ngayPhatHanh) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.ngayPhatHanh = ngayPhatHanh